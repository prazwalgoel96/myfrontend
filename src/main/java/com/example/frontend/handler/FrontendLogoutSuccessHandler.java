package com.example.frontend.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import com.example.frontend.model.Token;
import com.example.frontend.service.TokenService;

public class FrontendLogoutSuccessHandler implements LogoutSuccessHandler {

	@Autowired
	Token token;
	
	@Autowired
	TokenService tokenservice;
	
	@Bean public Token token() {
		return token; }
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();	

	@Override
	public void onLogoutSuccess(HttpServletRequest Request, HttpServletResponse Response, Authentication authentication)
			throws IOException, ServletException {
		if(token.role.equals("ROLE_ADMIN"))
		{
//			tokenservice.revokeToken();
			token.role = null;
//			System.out.print("\nAfter Revoking access Token" + token.access_token);
			redirectStrategy.sendRedirect(Request, Response, "/");
		}
		else if(token.role.equals("ROLE_USER"))
		{
			token.role = null;
//			System.out.print("\nAccess Token for tester" + token.access_token);
			redirectStrategy.sendRedirect(Request, Response, "/");
			
		}else {
			throw new IllegalStateException();
		}
	}
}
