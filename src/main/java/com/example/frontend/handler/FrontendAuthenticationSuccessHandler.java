package com.example.frontend.handler;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.example.frontend.service.TokenService;

public class FrontendAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	
	@Autowired
	TokenService tokenservice;

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest Request, HttpServletResponse Response, Authentication authentication)
			throws IOException, ServletException {
		
		boolean hasUserRole = false;
		boolean hasAdminRole = false;
		
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		for (GrantedAuthority grantedAuthority : authorities) {
			if (grantedAuthority.getAuthority().equals("ROLE_USER")) {
				hasUserRole = true;
				break;
			} else if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
				hasAdminRole = true;
				break;
			}
		}
		
		if (hasUserRole) {
			System.out.print("\nROLE_USER");
			tokenservice.setRole("ROLE_USER");
			redirectStrategy.sendRedirect(Request, Response, "/");
		} else if (hasAdminRole) {
			System.out.print("\nROLE_ADMIN");
			tokenservice.setRole("ROLE_ADMIN");
//			tokenservice.generateToken();
			redirectStrategy.sendRedirect(Request, Response, "/");
		} else {
			throw new IllegalStateException();
		}
	}

}
