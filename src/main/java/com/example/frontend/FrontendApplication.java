package com.example.frontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
//import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

//@EnableFeignClients
// @EnableDiscoveryClient
//@EnableEurekaClient
@SpringBootApplication
@ComponentScan("com.example.frontend")
public class FrontendApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(FrontendApplication.class, args);
	}

	    @Bean
    	public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
   	 }
	/*
	 * @Bean public RestTemplate restTemplate() { return new RestTemplate(); }
	 */

}
