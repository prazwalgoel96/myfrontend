package com.example.frontend.controller;



import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.*;
import org.springframework.ui.Model;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import com.example.frontend.model.Token;


//import com.example.callhelloworld.config.FeignClientHelloWorld;

@RefreshScope
@RestController
@CrossOrigin(origins = "*")
public class FrontendController {

	@Autowired
    private RestTemplate restTemplate;
	
	@Autowired
	Token token;

	@RequestMapping("/")
	public ModelAndView rootpage(){
		return new ModelAndView("welcome");
	}
	
	@RequestMapping("/test")
	public String testing() throws JSONException {
		String s = "{\"phonetype\":\"Testing Frontend\",\"cat\":\"WP\"}";
		HashMap map = new HashMap();
		JSONObject jo = new JSONObject(s);;
		try {
			System.out.println("\n{\n");
			Iterator<?> keys = jo.keys();
	        while( keys.hasNext() ){
	            String key = (String)keys.next();
	            String value = jo.getString(key);
	            map.put(key, value);
	            System.out.println("\""+ key + "\" : \"" + value + "\"");
	        }
	        System.out.println("\n}");
	        System.out.println("\n{\n");
	        String b = "Custom";
	        Object a = (Object) b;
	        jo.put("Handler", a);
	        System.out.println("\nHelloHelloHello");
	        System.out.println("\n"+ jo);
	        
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return s;
	}

//	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(path = "/call", produces = "application/json")
	public Object callhello() {
		
//		HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.add("User-Agent", "Spring's RestTemplate" );
//        headers.add("Authorization", "Bearer "+ token.access_token );
//        
//        HttpEntity<String> request = new HttpEntity<String>(headers);
		
		String url = "http://localhost:8082/prajwal/get";
		ResponseEntity<Object> entity = restTemplate.getForEntity(url, Object.class);
		Object result = entity.getBody();
		Map obj = (Map) result;
		System.out.println("\n{");
		Iterator<Map.Entry> itr1 = obj.entrySet().iterator(); 
        while (itr1.hasNext()) { 
            Map.Entry pair = itr1.next(); 
            System.out.println("\n"+ pair.getKey() + " : " + pair.getValue()); 
        } 

		
		System.out.println("\n}");
        return result;
		
	}
	
	@RequestMapping(path = "/facebook")
	public RedirectView facebookLogin() {
		
//		String url = "https://34.87.53.150:8243/authorize";
//		url += "?response_type=code&client_id=A87PAmTl2c6l6LDK8qXEmABHubwa&redirect_uri=http://localhost:8083";
//		ResponseEntity<String> entity = restTemplate.getForEntity(url, String.class);
//		String result = entity.getBody();
//		System.out.println("\nFacebook Login response " + result);
//      return result;
		RedirectView redirectView = new RedirectView();
		redirectView.setUrl("https://34.87.53.150:8243/authorize?response_type=code&client_id=A87PAmTl2c6l6LDK8qXEmABHubwa&redirect_uri=http://localhost:8083");
		return redirectView;
		
	}
	
	@GetMapping(path = "/token")
	public String token() {
		
        return token.result;
		
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(Model model, String error, String logout){
		
        if (error != null)
            model.addAttribute("errorMsg", "Your username and password are invalid.");

        if (logout != null)
            model.addAttribute("msg", "You have been logged out successfully.");

        return new ModelAndView("login");
	}
}
