package com.example.frontend.config;

//import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.example.frontend.handler.FrontendAuthenticationSuccessHandler;
import com.example.frontend.handler.FrontendLogoutSuccessHandler;

@Configuration
@EnableWebSecurity
public class FrontendSecurityConfiguration extends WebSecurityConfigurerAdapter {

//	@Autowired
//    DataSource dataSource;
	
	@Autowired
	FrontendAuthenticationSuccessHandler successHandler;
	
	@Bean public FrontendAuthenticationSuccessHandler successHandler() { return new FrontendAuthenticationSuccessHandler(); }

	@Bean
	public FrontendLogoutSuccessHandler logoutSuccessHandler() {
	    return new FrontendLogoutSuccessHandler();
	}
	
	//Enable jdbc authentication
//    @Autowired
//    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
//        auth.jdbcAuthentication().dataSource(dataSource);
//    }
	
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/").hasAnyRole("USER", "ADMIN").antMatchers("/welcome")
            .hasAnyRole("USER", "ADMIN").antMatchers("/test").hasAnyRole("USER", "ADMIN")
            .antMatchers("/call").hasAnyRole("ADMIN").anyRequest().authenticated().and().formLogin().successHandler(successHandler).loginPage("/login")
            .permitAll().and().logout().logoutSuccessHandler(logoutSuccessHandler()).permitAll();

        http.csrf().disable();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authenticationMgr) throws Exception {
        authenticationMgr.inMemoryAuthentication().withUser("tester").password("{noop}tester")
            .authorities("ROLE_USER").and().withUser("admin").password("{noop}admin")
            .authorities("ROLE_USER", "ROLE_ADMIN");
    }

}
