package com.example.frontend.service;

import java.util.Arrays;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.frontend.model.Token;
import com.example.frontend.service.TokenService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class TokenServiceImpl implements TokenService {

	@Autowired
	Token token;
	
	@Bean public Token token() {
		return new Token();  }

	RestTemplate restTemplate = new RestTemplate();
	
	@Override
	public void generateToken() {
		
		
		String credentials = "AHtoPC2cwjN6e6gtr5Z5IdmxKGsa:nR1HH9li8frAEg4SWm6rDcrM0q8a";
		String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Authorization", "Basic " + encodedCredentials);
		headers.add("Content-Type", "application/x-www-form-urlencoded");
		
		HttpEntity<String> request = new HttpEntity<String>(headers);
		
		String url = "https://gw-wso2-platform.tk/token";
		url += "?grant_type=password&username=admin&password=admin";
		ResponseEntity<String> entity = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
		token.result = entity.getBody();
		System.out.println("\nRESPONSE " + token.result);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = null;
		try {
			node = mapper.readTree(entity.getBody());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		token.access_token = node.path("access_token").asText();
		System.out.println("\nAccess Token " + token.access_token);
		
	}


	@Override
	public void revokeToken() {
		
		String credentials = "AHtoPC2cwjN6e6gtr5Z5IdmxKGsa:nR1HH9li8frAEg4SWm6rDcrM0q8a";
		String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Authorization", "Basic " + encodedCredentials);
		headers.add("Content-Type", "application/x-www-form-urlencoded");
		
		HttpEntity<String> request = new HttpEntity<String>(headers);
		
		String url = "https://gw-wso2-platform.tk/revoke";
		url += "?token=" + token.access_token;
		ResponseEntity<String> entity1 = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
		System.out.print("\nRevoked ----- " + entity1);
		token.result = null;
		token.access_token = null;
		
	}


	@Override
	public String getRole() {
		return token.role;
	}


	@Override
	public String getAccessToken() {
		return token.access_token;
	}



	@Override
	public void setRole(String a) {
		token.role = a;
	}

}
