package com.example.frontend.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface TokenService {

	void generateToken() throws JsonMappingException, JsonProcessingException;
	void revokeToken();
	String getRole();
	String getAccessToken();
	void setRole(String a);
}
