<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div style="border: 1px solid #ccc; padding: 5px; margin-bottom: 20px;">

	<a href="/">Home</a> |  

	<a href="/test">Test
		Frontend</a> |   <a
		href="/call">Call
		WSO2 API</a> |   <a
		href="/token">Generate
		Access Token</a> | <a onclick="document.forms['logoutForm'].submit()">Logout</a>

    <form id="logoutForm" method="POST" action="/logout">
    </form>

</div>